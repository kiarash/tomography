package tomography;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This class provides static methods to verify whether a binary matrix can be
 * reconstructed from two given lists of row and column totals.
 * 
 * @author kiarash
 */
public class BinaryMatrix {

  public static final int MIN_SIZE = 1;
  public static final int MAX_SIZE = 1000;

  /**
   * args[0] must be an input file path.
   * 
   * @param args
   */
  public static void main(String[] args) {
    if (args.length < 1) {
      System.out.println("Please specify an input file.");
      return;
    }

    try {
      if (BinaryMatrix.exists(args[0]))
        System.out.println("Yes");
      else
        System.out.println("No");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Returns true or false indicating whether there exists a matrix with the
   * given row totals and column totals projections.
   * <p>
   * Invokes {@link BinaryMatrix#exists(int[], int[])} beneath to determines
   * existence of solution.
   * 
   * @param inputPath
   *          input parameter file
   * @return true if there exist at least one binary matrix with given rows and
   *         columns projections, false otherwise
   * @throws FileNotFoundException
   */
  public static boolean exists(String inputPath) throws FileNotFoundException {
    if (inputPath == null || inputPath.isEmpty())
      throw new IllegalArgumentException("Input path is empty");

    int[][] rowColumTotals = readInputs(inputPath);
    return BinaryMatrix.exists(rowColumTotals[0], rowColumTotals[1]);
  }

  /**
   * Returns true or false indicating whether there exists a matrix with the
   * given row totals and column totals projections.
   * <p>
   * The implementation is based on the Havel-Hakimi theorem (Havel 1955, Hakimi
   * 1962) stating that there exists a matrix Xn,m of 0�s and 1�s with row
   * totals a0 = (a1, a2,..., an) and column totals b0 5 (b1, b2,..., bm) such
   * that bi >= bi+1 for every 0 , i , m if and only if another matrix Xn-1,m of
   * 0�s and 1�s with row totals a1 = (a2, a3,..., an) and column totals b1 =
   * (b1-2, b2-1, . . . , b(a1-1) 1, b(a1), ..., bm) also exists. Using the
   * Havel-Hakimi theorem, we reduce the problem of the existence of a matrix
   * with given row and column totals to the problem of the existence of a
   * smaller matrix.
   * 
   * @param rt
   *          Array of integers referring to row totals
   * @param ct
   *          Array of integers referring to column totals
   * @return true if there exist at least one binary matrix with given rows and
   *         columns projections, false otherwise
   * @throws IllegalArgumentException
   *           if any invalid input array argument is found
   */
  public static boolean exists(int[] rt, int[] ct) {
    if (rt == null || rt.length == 0 || ct == null || ct.length == 0)
      throw new IllegalArgumentException("input arrays must be non-empty");

    // detecting an obvious case of non-existent solution at the early stage of
    // processing, inequality of row and column sums
    int rowSum = 0, columnSum = 0;
    for (int r : rt) {
      if (r < 0)
        throw new IllegalArgumentException("row total values can not be negative");
      rowSum += r;
    }
    for (int c : ct) {
      if (c < 0)
        throw new IllegalArgumentException("column total values can not be negative");
      columnSum += c;
    }
    if (rowSum != columnSum)
      return false;

    // copy primitive int array to object Integer array to avoid
    // manipulating inputs and make use of Arrays sorting util
    Integer[] ctDesc = new Integer[ct.length];
    for (int i = 0; i < ct.length; i++)
      ctDesc[i] = ct[i];

    // to avoid sorting unnecessary elements in the sorted array
    int sortToIndex = ctDesc.length;

    for (int i = 0; i < rt.length; i++) {
      Arrays.sort(ctDesc, 0, sortToIndex, Collections.reverseOrder());

      if (rt[i] == 0)
        continue;

      // row total can not be greater than number of columns
      if (rt[i] > ct.length)
        return false;

      // insufficient number of 1 columns to sum up to the row total
      if (ctDesc[rt[i] - 1] == 0)
        return false;

      // reduce columns total array to a correspondent total column arrays
      // of Matrix of size m-1 rows n columns (after removal of r[i])
      for (int j = 0; j < rt[i]; j++)
        ctDesc[j] = ctDesc[j] - 1;

      // update upper boundary of elements to be sorted
      for (int k = sortToIndex - 1; k >= 0; k--) {
        if (ctDesc[k] == 0)
          sortToIndex = sortToIndex - 1;
        else
          break;
      }
    }

    // since equality of row and columns totals sum has been checked, by this
    // point all array elements of column total must have been reduced to zero
    for (Integer elem : ctDesc)
      assert (elem == 0);

    return true;
  }

  /**
   * Reads rows and columns totals from the given file path and returns as a two
   * dimensional int array
   * <p>
   * Expected file format: <br>
   * m n // number of rows and column<br>
   * r1 r2 ... rm // row totals<br>
   * c1 c2 ... cn //column totals <br>
   * 
   * @param filePath
   *          input parameter file
   * @return two dimensional int array containing row and column totals,
   *         result[0] refer to array of row totals and result[1] refer to array
   *         of column totals
   * @throws FileNotFoundException
   * @throws InputMismatchException
   */
  private static int[][] readInputs(String filePath) throws FileNotFoundException {
    Scanner reader = null;

    try {
      reader = new Scanner(new BufferedReader(new FileReader(filePath)));

      int rowSize = reader.nextInt();
      int columnSize = reader.nextInt();

      if (rowSize < MIN_SIZE || rowSize > MAX_SIZE || columnSize < MIN_SIZE || columnSize > MAX_SIZE)
        throw new InputMismatchException(String.format("Matrix size must be between %dx%d and %dx%d", MIN_SIZE,
            MIN_SIZE, MAX_SIZE, MAX_SIZE));

      int[][] rowColumnTotals = new int[][] { new int[rowSize], new int[columnSize] };
      for (int i = 0; i < rowSize; i++) {
        if ((rowColumnTotals[0][i] = reader.nextInt()) < 0)
          throw new InputMismatchException("Total values can not be negative");
      }

      for (int i = 0; i < columnSize; i++)
        if ((rowColumnTotals[1][i] = reader.nextInt()) < 0)
          throw new InputMismatchException("Total values can not be negative");

      if (reader.hasNextInt())
        throw new InputMismatchException("Surplus elements of rows/colums total");

      return rowColumnTotals;

    } finally {
      if (reader != null)
        reader.close();
    }
  }
}
