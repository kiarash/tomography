package tomography;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.InputMismatchException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

public class BinaryMatrixTest {
  @Rule
  public ExpectedException exception = ExpectedException.none();
  @Rule
  public TemporaryFolder tempFolder = new TemporaryFolder();

  @Test
  public void exists_intArrays_onValidSums_returnsTrue() {
    int[] rt0 = { 1 };
    int[] ct0 = { 1 };
    assertTrue(BinaryMatrix.exists(rt0, ct0));

    int[] rt1 = { 0, 0, 0 };
    int[] ct1 = { 0, 0, 0 };
    assertTrue(BinaryMatrix.exists(rt1, ct1));

    int[] rt2 = { 3, 3, 3 };
    int[] ct2 = { 3, 3, 3 };
    assertTrue(BinaryMatrix.exists(rt2, ct2));

    int[] rt3 = { 2, 3, 2 };
    int[] ct3 = { 1, 1, 3, 2 };
    assertTrue(BinaryMatrix.exists(rt3, ct3));
  }

  @Test
  public void exists_intArrays_onInvalidSums_returnsFlase() {
    int[] rt0 = { 1 };
    int[] ct0 = { 2 };
    assertFalse(BinaryMatrix.exists(rt0, ct0));

    int[] rt1 = { 4, 4, 4 };
    int[] ct1 = { 4, 4, 4 };
    assertFalse(BinaryMatrix.exists(rt1, ct1));

    int[] rt2 = { 3, 3, 1 };
    int[] ct2 = { 3, 3, 1 };
    assertFalse(BinaryMatrix.exists(rt2, ct2));

    int[] rt3 = { 1, 1, 3 };
    int[] ct3 = { 0, 2, 3 };
    assertFalse(BinaryMatrix.exists(rt3, ct3));

    int[] rt4 = { 1, 1, 1 };
    int[] ct4 = { 1, 1, 0 };
    assertFalse(BinaryMatrix.exists(rt4, ct4));
  }

  @Test
  public void exists_intArrays_onNegativeValue1_throwsIllegalArgumentException() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("negative");
    BinaryMatrix.exists(new int[] { 1, -1 }, new int[] { 1, 1 });
  }

  @Test
  public void exists_intArrays_onNegativeValue2_throwsIllegalArgumentException() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("negative");
    BinaryMatrix.exists(new int[] { 1, 1 }, new int[] { 1, -1 });
  }

  @Test
  public void exists_intArrays_onEmpty1_throwsIllegalArgumentException() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("empty");
    BinaryMatrix.exists(null, new int[] { 1, 2 });
  }

  @Test
  public void exists_intArrays_onEmpty2_throwsIllegalArgumentException() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("empty");
    BinaryMatrix.exists(new int[] { 1, 2 }, null);
  }

  @Test
  public void exists_intArrays_onEmpty3_throwsIllegalArgumentException() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("empty");
    BinaryMatrix.exists(new int[] { 1 }, new int[] {});
  }

  @Test
  public void exists_intArrays_onEmpty4_throwsIllegalArgumentException() {
    exception.expect(IllegalArgumentException.class);
    exception.expectMessage("empty");
    BinaryMatrix.exists(new int[] {}, new int[] { 1 });
  }

  @Test
  public void exists_string_onValidSums_returnTrue() throws IOException {
    String input = "3 4\n" + "2 3 2\n" + "1 1 3 2\n";
    File validFile = null;
    try {
      validFile = tempFolder.newFile("validInputs");
      write(validFile, input);
      assertTrue(BinaryMatrix.exists(validFile.getPath()));
    } finally {
      if (validFile != null)
        validFile.delete();
    }
  }

  @Test
  public void exists_string_onInvalidSums_returnsFalse() throws IOException {
    String input = "3 3\n" + "0 0 3\n" + "0 0 3\n";
    File validFile = null;
    try {
      validFile = tempFolder.newFile("invalidInputs");
      write(validFile, input);
      assertFalse(BinaryMatrix.exists(validFile.getPath()));
    } finally {
      if (validFile != null)
        validFile.delete();
    }
  }

  @Test
  public void exists_string_onBadInput_throwsMisMatchedInputException() throws IOException {
    exception.expect(InputMismatchException.class);
    exception.expectMessage("Surplus elements");

    String input = "3 3\n" + "0 0 3\n" + "0 0 3 0\n";
    File inputFile = null;
    try {
      inputFile = tempFolder.newFile("inputs");
      write(inputFile, input);
      BinaryMatrix.exists(inputFile.getPath());
    } finally {
      if (inputFile != null)
        inputFile.delete();
    }
  }

  private void write(File file, String txt) throws IOException {
    BufferedWriter out = null;
    try {
      out = new BufferedWriter(new FileWriter(file));
      out.write(txt);
      out.flush();
    } finally {
      if (out != null)
        out.close();
    }
  }
}
